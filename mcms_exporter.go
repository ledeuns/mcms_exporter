package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os/user"
	"time"
	"strings"
	"strconv"

	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"github.com/prometheus/common/promlog"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsonrw"
)

const namespace = "mcms"

type Exporter struct { }

type GPON struct {
	Id string `bson:"_id"`
	GPON struct {
		Temperature float64 `bson:"Temperature"`
		MAC string `bson:"Host MAC Address"`
		ONUID string `bson:"Equipment ID"`
	} `bson:"GPON"`
	CNTL struct {
		MAC string `bson:"MAC Address"`
	} `bson:"CNTL"`
	STATS struct {
		ONUPON struct {
			TXLevel float64 `bson:"TX Optical Level"`
			RXLevel float64 `bson:"RX Optical Level"`
		} `bson:"ONU-PON"`
		ONUUNIETH struct {
			Uptime float64 `bson:"Uptime"`
			TotalTX float64 `bson:"TX Total Octets"`
			TotalRX float64 `bson:"RX Total Octets"`
			RateTX float64 `bson:"TX Rate bps"`
			RateRX float64 `bson:"RX Rate bps"`
		} `bson:"ONU-UNI-ETH 1"`
	} `bson:"STATS"`
	OLT struct {
		MAC string `bson:"MAC Address"`
	} `bson:"OLT"`
}

var (
	listenAddress = flag.String("web.listen-address", ":8998",
		"Address on which to expose metrics and web interface")
	metricsPath = flag.String("web.telemetry-path", "/metrics",
		"Path under which to expose metrics")
	mcmsLabels = []string{"controller", "olt", "onuid"}

	// Metrics
	up = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "up"),
		"Was the last mongodb query successful",
		nil, nil,)
	onuUptime = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_uptime"),
		"ONU Uptime",
		mcmsLabels, nil,)
	onuTXlevel = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_tx_level"),
		"ONU TX power (dB)",
		mcmsLabels, nil,)
	onuRXlevel = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_rx_level"),
		"ONU RX power (dB)",
		mcmsLabels, nil,)
	onuTXtotal = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_tx_bytes_total"),
		"ONU transmitted bytes",
		mcmsLabels, nil,)
	onuRXtotal = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_rx_bytes_total"),
		"ONU received bytes",
		mcmsLabels, nil,)
	onuTXrate = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_tx_rate"),
		"ONU transmit rate (bps)",
		mcmsLabels, nil,)
	onuRXrate = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "onu_rx_rate"),
		"ONU receive rate (bps)",
		mcmsLabels, nil,)
)

func NewExporter() *Exporter {
	return &Exporter{ }
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- up
	ch <- onuUptime
	ch <- onuTXlevel
	ch <- onuRXlevel
	ch <- onuRXrate
	ch <- onuTXrate
	ch <- onuTXtotal
	ch <- onuRXtotal
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	var onustate, err = e.getOnuState()
	if err != nil {
		ch <- prometheus.MustNewConstMetric(
			up, prometheus.GaugeValue, 0,)
		log.Println(err)
		return
	}

	ch <- prometheus.MustNewConstMetric(
		up, prometheus.GaugeValue, 1,)
	e.UpdateMetrics(onustate, ch)
}

func (e *Exporter) UpdateMetrics(state []bson.M, ch chan<- prometheus.Metric) {
	var gpon GPON

	for _, result := range state {
		data, _ := bson.Marshal(result)
		decoder, _ := bson.NewDecoder(bsonrw.NewBSONDocumentReader(data))
		decoder.Decode(&gpon)
		ch <- prometheus.MustNewConstMetric(onuUptime, prometheus.GaugeValue, gpon.STATS.ONUUNIETH.Uptime, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuTXlevel, prometheus.GaugeValue, gpon.STATS.ONUPON.TXLevel, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuRXlevel, prometheus.GaugeValue, gpon.STATS.ONUPON.RXLevel, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuRXrate, prometheus.GaugeValue, gpon.STATS.ONUUNIETH.RateRX, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuTXrate, prometheus.GaugeValue, gpon.STATS.ONUUNIETH.RateTX, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuTXtotal, prometheus.GaugeValue, gpon.STATS.ONUUNIETH.TotalTX, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
		ch <- prometheus.MustNewConstMetric(onuRXtotal, prometheus.GaugeValue, gpon.STATS.ONUUNIETH.TotalRX, gpon.CNTL.MAC, gpon.OLT.MAC, gpon.GPON.ONUID)
	}
}

func convertMAC(mac string) (uint64, error) {
	return strconv.ParseUint(strings.Replace(mac, ":", "", -1), 16, 64)
}

func (e *Exporter) getOnuState() ([]bson.M, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		return nil, err
	}
	myCollection := client.Database("tibit_pon_controller").Collection("ONU-STATE")
	cursor, err := myCollection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}

	return results, nil
}

func main() {
	promlogConfig := &promlog.Config{}
	flag.Parse()

	logger := promlog.New(promlogConfig)

	exporter := NewExporter()
	prometheus.MustRegister(exporter)

	level.Info(logger).Log("msg", "Starting mcms_exporter", "version", version.Info())
	level.Info(logger).Log("msg", "Build context", "build_context", version.BuildContext())
	if user, err := user.Current(); err == nil && user.Uid == "0" {
		level.Warn(logger).Log("msg", "MCMS Exporter is running as root user. This exporter is designed to run as unpriviledged user, root is not required.")
	}

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
	     <head><title>Tibit MCMS Exporter</title></head>
	     <body>
	     <h1>Tibit MCMS Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	level.Info(logger).Log("msg", "Listening on", "address", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
