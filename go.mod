module mcms_exporter

go 1.13

require (
	github.com/go-kit/log v0.2.1
	github.com/prometheus/client_golang v1.19.0
	github.com/prometheus/common v0.52.3
	go.mongodb.org/mongo-driver v1.15.0
)
